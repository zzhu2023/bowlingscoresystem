package com.sparebank1.bowling;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BowlingController {

  @PostMapping("/score")
  public ResponseEntity<Integer> score(@RequestBody ScoreSheet scoreSheet) {
    BowlingGame game = new BowlingGame();
    scoreSheet.getRolls().forEach(pins -> game.roll(pins));
    return new ResponseEntity<>(game.score(), HttpStatus.OK);
  }
}
