package com.sparebank1.bowling;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BowlingGame {
  private List<Frame> frames = new ArrayList<>();
  private int currentFrameIndex;

  public BowlingGame() {
    currentFrameIndex = 0;
    frames.add(new Frame(currentFrameIndex, frames));
  }

  public void roll(int pins) {
    Frame currentFrame = frames.get(currentFrameIndex);
    if (currentFrame.isComplete()) {
      currentFrameIndex++;
      if (currentFrameIndex == frames.size()) {
        frames.add(new Frame(currentFrameIndex, frames));
      }
    }
    frames.get(currentFrameIndex).roll(pins);
  }

  public int score() {
    return frames.stream().mapToInt(Frame::score).sum();
  }

  public List<Integer> frameScores() {
    return frames.stream().map(Frame::score).collect(Collectors.toList());
  }
}
