package com.sparebank1.bowling;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Frame {
  private static final int MAX_PINS = 10;
  private static final int TOTAL_FRAMES = 10;
  private static final int REGULAR_FRAME_SIZE = 2;
  private static final int FINAL_FRAME_SIZE = 3;

  private int index;
  private List<Frame> frames;
  private List<Integer> rolls = new ArrayList<>();

  public Frame(int index, List<Frame> frames) {
    this.index = index;
    this.frames = frames;
  }

  public void roll(int pins) {
    rolls.add(pins);
  }

  public boolean isComplete() {
    boolean isRegularFrameComplete =
        !isFinalFrame() && (rolls.size() == REGULAR_FRAME_SIZE || isStrike());

    boolean hasNoBonusRoll = rolls.size() == REGULAR_FRAME_SIZE && score() < MAX_PINS;
    boolean hasBonusRoll = (rolls.size() == FINAL_FRAME_SIZE) && (isStrike() || isSpare());
    boolean isFinalFrameComplete = isFinalFrame() && (hasBonusRoll || hasNoBonusRoll);

    return isFinalFrameComplete || isRegularFrameComplete;
  }

  private boolean isFinalFrame() {
    return index == TOTAL_FRAMES - 1;
  }

  private boolean isStrike() {
    return rolls.size() == 1 && rolls.get(0) == MAX_PINS;
  }

  private boolean isSpare() {
    return rolls.size() == REGULAR_FRAME_SIZE && rolls.get(0) + rolls.get(1) == MAX_PINS;
  }

  public int score() {
    int baseScore = rolls.stream().mapToInt(Integer::intValue).sum();

    if (isStrike()) {
      return baseScore + bonusForStrike();
    }

    if (isSpare()) {
      return baseScore + bonusForSpare();
    }

    return baseScore;
  }

  private int bonusForStrike() {
    Optional<Frame> nextFrameOptional = nextFrame();
    if (nextFrameOptional.isPresent()) {
      Frame nextFrame = nextFrameOptional.get();
      Optional<Frame> secondNextFrameOptional = secondNextFrame();

      if (nextFrame.isStrike() && secondNextFrameOptional.isPresent()) {
        Frame secondNextFrame = secondNextFrameOptional.get();
        return MAX_PINS + secondNextFrame.rolls.get(0);
      } else {
        return nextFrame.rolls.stream().limit(REGULAR_FRAME_SIZE).mapToInt(Integer::intValue).sum();
      }
    }
    return 0;
  }

  private int bonusForSpare() {
    return nextFrame().map(frame -> frame.rolls.get(0)).orElse(0);
  }

  private Optional<Frame> nextFrame() {
    if (frames.size() >= index + 2) {
      return Optional.of(frames.get(index + 1));
    }
    return Optional.empty();
  }

  private Optional<Frame> secondNextFrame() {
    if (frames.size() >= index + 3) {
      return Optional.of(frames.get(index + 2));
    }
    return Optional.empty();
  }
}
