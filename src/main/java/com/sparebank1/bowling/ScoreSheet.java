package com.sparebank1.bowling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ScoreSheet {
  private static final String STRIKE = "X";
  private static final String SPARE = "/";
  private static final String MISS = "-";

  private String scoreAsString;

  public ScoreSheet() {}

  public String getScoreAsString() {
    return scoreAsString;
  }

  public void setScoreAsString(String scoreAsString) {
    this.scoreAsString = scoreAsString;
  }

  public ScoreSheet(String scoreAsString) {
    this.scoreAsString = scoreAsString;
  }

  public List<Integer> getRolls() {
    List<String> scoreSymbols = Arrays.asList(scoreAsString.replaceAll("\\s", "").split("(?!^)"));

    List<Integer> rolls = new ArrayList<>();
    for (int i = 0; i < scoreSymbols.size(); i++) {
      String symbol = scoreSymbols.get(i);
      switch (symbol) {
        case STRIKE:
          rolls.add(10);
          break;
        case MISS:
          rolls.add(0);
          break;
        case SPARE:
          rolls.add(10 - Integer.valueOf(scoreSymbols.get(i - 1)));
          break;
        default:
          rolls.add(Integer.valueOf(symbol));
      }
    }
    return rolls;
  }
}
