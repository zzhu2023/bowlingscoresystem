package com.sparebank1.bowling;

import java.util.stream.Stream;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

class BowlingGameTest {

  @Test
  public void shouldScoreBe300_whenPerfectGame() {
    BowlingGame game = new BowlingGame();

    Stream.of(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10).forEach(pins -> game.roll(pins));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(game.score()).isEqualTo(300);
    softly.assertAll();
  }

  @Test
  public void shouldScoreBe90_when10PairsOf9AndMisses() {
    BowlingGame game = new BowlingGame();
    Stream.of(9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0)
        .forEach(pins -> game.roll(pins));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(game.score()).isEqualTo(90);
    softly.assertAll();
  }

  @Test
  public void shouldScoreBe150_when10PairsOf5AndSpareAndFinal5() {
    BowlingGame game = new BowlingGame();
    Stream.of(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5)
        .forEach(pins -> game.roll(pins));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(game.score()).isEqualTo(150);
    softly.assertAll();
  }

  @Test
  public void shouldScoreBe170_whenScoreSheet1() {
    BowlingGame game = new BowlingGame();
    Stream.of(8, 2, 7, 3, 3, 4, 10, 2, 8, 10, 10, 8, 0, 10, 8, 2, 9)
        .forEach(pins -> game.roll(pins));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(game.score()).isEqualTo(170);
    softly.assertAll();
  }

  @Test
  public void shouldScoreBe110_whenScoreSheet2() {
    BowlingGame game = new BowlingGame();
    Stream.of(8, 1, 6, 4, 8, 2, 4, 0, 10, 6, 3, 8, 2, 7, 2, 2, 0, 5, 4)
        .forEach(pins -> game.roll(pins));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(game.score()).isEqualTo(110);
    softly.assertAll();
  }

  @Test
  public void shouldScoreBe110_whenScoreSheet3() {
    BowlingGame game = new BowlingGame();
    Stream.of(10, 10, 7, 3, 8, 1, 10, 4, 2, 5, 4, 7, 3, 9, 0, 10, 10, 7)
        .forEach(pins -> game.roll(pins));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(game.score()).isEqualTo(160);
    softly.assertAll();
  }
}
