package com.sparebank1.bowling;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

class ScoreSheetTest {
  @Test
  public void shouldCorrespond_whenSample01() {
    SoftAssertions softly = new SoftAssertions();
    ScoreSheet parser = new ScoreSheet("X X X X X X X X X X X X");
    softly
        .assertThat(parser.getRolls())
        .containsExactly(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);

    softly.assertAll();
  }

  @Test
  public void shouldCorrespond_whenSample02() {
    SoftAssertions softly = new SoftAssertions();
    ScoreSheet parser = new ScoreSheet("9- 9- 9- 9- 9- 9- 9- 9- 9- 9-");
    softly
        .assertThat(parser.getRolls())
        .containsExactly(9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0);

    softly.assertAll();
  }

  @Test
  public void shouldCorrespond_whenSample03() {
    SoftAssertions softly = new SoftAssertions();
    ScoreSheet parser = new ScoreSheet("5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5");
    softly
        .assertThat(parser.getRolls())
        .containsExactly(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5);

    softly.assertAll();
  }

  @Test
  public void shouldCorrespond_whenSample04() {
    SoftAssertions softly = new SoftAssertions();
    ScoreSheet parser = new ScoreSheet("8/ 7/ 34 X 2/ X X 8- X 8/9");
    softly
        .assertThat(parser.getRolls())
        .containsExactly(8, 2, 7, 3, 3, 4, 10, 2, 8, 10, 10, 8, 0, 10, 8, 2, 9);

    softly.assertAll();
  }

  @Test
  public void shouldCorrespond_whenSample05() {
    SoftAssertions softly = new SoftAssertions();
    ScoreSheet parser = new ScoreSheet("81 6/ 8/ 4- X 63 8/ 72 2- 54");
    softly
        .assertThat(parser.getRolls())
        .containsExactly(8, 1, 6, 4, 8, 2, 4, 0, 10, 6, 3, 8, 2, 7, 2, 2, 0, 5, 4);

    softly.assertAll();
  }

  @Test
  public void shouldCorrespond_whenSample06() {
    SoftAssertions softly = new SoftAssertions();
    ScoreSheet parser = new ScoreSheet("X X 7/ 81 X 42 54 7/ 9- XX7");
    softly
        .assertThat(parser.getRolls())
        .containsExactly(10, 10, 7, 3, 8, 1, 10, 4, 2, 5, 4, 7, 3, 9, 0, 10, 10, 7);

    softly.assertAll();
  }
}
